package com.example.guess_number_2

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    companion object {
        const val TAG = "MainActivity"
    }
    private val sharedPreferences by lazy {
        getSharedPreferences("guess", Context.MODE_PRIVATE)
    }
    private val guessGame = GuessGame()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val times = sharedPreferences.getInt("times", 0)
        if (times != 0) {
            AlertDialog.Builder(this)
                .setTitle("Guess")
                .setMessage("繼續上次進度")
                .setPositiveButton("yes") { _, _ ->
                    load()
                }
                .setNegativeButton("no", null)
                .show()
        }

        guessButton.setOnClickListener {
            val numberStr = numberEditText.text.toString()
            val number = numberStr.toIntOrNull() ?: run {
                Log.d(TAG, "輸入 = $numberStr, 無法轉成整數")
                return@setOnClickListener
            }
            val isCorrect = guessGame.guess(number)
            val message = guessGame.message

            AlertDialog.Builder(this).apply {
                setTitle("Guess")
                setMessage(message)
                if (isCorrect) {
                    setPositiveButton("replay") { _, _ ->
                        replay()
                    }
                }
                setNeutralButton("ok", null)
            }.show()
            guessGame.leftNumber.toString() .also { leftNumberTextView.text = it }
            guessGame.rightNumber.toString().also { rightNumberTextView.text = it }
            "第 ${guessGame.times} 次"      .also { timesTextView.text = it }
        }

        replayButton.setOnClickListener {
            AlertDialog.Builder(this)
                .setTitle("Guess")
                .setMessage("是否重玩")
                .setPositiveButton("yes") { _, _ ->
                    replay()
                }
                .setNeutralButton("cancel", null)
                .show()
        }
    }

    private fun replay() {
        guessGame.reset()
        guessGame.leftNumber.toString() .also { leftNumberTextView.text = it }
        guessGame.rightNumber.toString().also { rightNumberTextView.text = it }
        "第 ${guessGame.times} 次"      .also { timesTextView.text = it }
    }

    private fun load() {
        sharedPreferences.apply {
            getInt("secret", 0)       .also { guessGame.secret = it }
            getInt("leftNumber", 1)   .also { guessGame.leftNumber = it }
            getInt("rightNumber", 100).also { guessGame.rightNumber = it }
            getInt("times", 0)        .also { guessGame.times = it }
        }
        guessGame.leftNumber.toString() .also { leftNumberTextView.text = it }
        guessGame.rightNumber.toString().also { rightNumberTextView.text = it }
        "第 ${guessGame.times} 次"      .also { timesTextView.text = it }
    }

    override fun onStop() {
        super.onStop()
        sharedPreferences.edit()
            .putInt("secret", guessGame.secret)
            .putInt("leftNumber", guessGame.leftNumber)
            .putInt("rightNumber", guessGame.rightNumber)
            .putInt("times", guessGame.times)
            .apply()
    }
}