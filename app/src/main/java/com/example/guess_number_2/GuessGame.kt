package com.example.guess_number_2

import java.util.*

class GuessGame {
    var secret = 0 // 2 ~ 99
    var leftNumber = 1
    var rightNumber = 100
    var times = 0
    var message = ""

    init {
        reset()
    }

    fun reset() {
        secret = Random().nextInt(98) + 2 // 2 ~ 99
        leftNumber = 1
        rightNumber = 100
        times = 0
        message = ""
    }

    fun guess(number: Int): Boolean {
        var isCorrect = false
        val diff = number - secret
        times++
        message = when {
            (diff > 0) -> {
                if (rightNumber > number)
                    rightNumber = number
                "清選擇數字 $leftNumber ~ $rightNumber"
            }
            (diff < 0) -> {
                if (leftNumber < number)
                    leftNumber = number
                "清選擇數字 $leftNumber ~ $rightNumber"
            }
            else -> {
                isCorrect = true
                "猜中了"
            }
        }
        message += ", 第 $times 次"
        return isCorrect
    }
}